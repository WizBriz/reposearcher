//
//  UserDataSource.swift
//  GithubScreening
//
//  Created by Wiz on 3/29/20.
//  Copyright © 2020 Owner. All rights reserved.
//

import Foundation
let searchURL = "https://api.github.com/search/users?q="

class UserDataSource {
    public static var shared : UserDataSource {
        let userdatasource = UserDataSource()
        return userdatasource
    }
    private init() {
    }
    
    func searchUser(keyword : String, updatedData : @escaping (Users?) -> Void) {
        NetworekManager.shared.getData(url: searchURL+keyword) { (data, response, error) in
            if let jsonData = data {
                do {
                    var users = try JSONDecoder().decode(Users.self, from: jsonData)
                    updatedData(users)
                } catch {
                    print(error.localizedDescription)
                    updatedData(nil)
                }
            }
        }
    }
}
