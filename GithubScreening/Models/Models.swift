//
//  User.swift
//  GithubScreening
//
//  Created by Wiz on 3/29/20.
//  Copyright © 2020 Owner. All rights reserved.
//

import Foundation
struct User : Codable {
    var login:String//"login": "prabhatk",
    var id:Int//"id": 246486,
    var node_id:String//"node_id": "MDQ6VXNlcjI0NjQ4Ng==",
    var avatar_url:String//"avatar_url": "https://avatars3.githubusercontent.com/u/246486?v=4",
    var gravatar_id:String//"gravatar_id": "",
    var url:String//"url": "https://api.github.com/users/prabhatk",
    var html_url:String//"html_url": "https://github.com/prabhatk",
    var followers_url:String//"followers_url": "https://api.github.com/users/prabhatk/followers",
    var following_url:String//"following_url": "https://api.github.com/users/prabhatk/following{/other_user}",
    var gists_url:String//"gists_url": "https://api.github.com/users/prabhatk/gists{/gist_id}",
    var starred_url:String//"starred_url": "https://api.github.com/users/prabhatk/starred{/owner}{/repo}",
    var subscriptions_url:String//"subscriptions_url": "https://api.github.com/users/prabhatk/subscriptions",
    var organizations_url:String//"organizations_url": "https://api.github.com/users/prabhatk/orgs",
    var repos_url:String//"repos_url": "https://api.github.com/users/prabhatk/repos",
    var events_url:String//"events_url": "https://api.github.com/users/prabhatk/events{/privacy}",
    var received_events_url:String//"received_events_url": "https://api.github.com/users/prabhatk/received_events",
    var type:String//"type": "User",
    var site_admin:Bool//"site_admin": false,
    var score:Float//"score": 1.0
}
struct Repo : Codable{
    var name:String//"name": "profile-rest-api"
    var html_url:String//"html_url": "https://github.com/prabhatk/profile-rest-api"
    var stargazers_count:Int//"stargazers_count": 0,
    var forks_count:Int//"forks_count": 0,
}
struct Users : Codable {
    var total_count : Int//"total_count": 160,
    var incomplete_results : Bool//"incomplete_results": false,
    var items : [User]//"items":
}
struct Profile : Codable {
    var login:String//"login": "prabhatk",
    var id:Int//"id": 246486,
    var avatar_url:String//"avatar_url": "https://avatars3.githubusercontent.com/u/246486?v=4",
    var repos_url:String//"repos_url": "https://api.github.com/users/prabhatk/repos",
    var name:String?//"name": null,
    var location:String?//"location": null,
    var email:String?//"email": null,
    var bio:String?//"bio": null,
    var public_repos:Int//"public_repos": 13,
    var public_gists:Int//"public_gists": 0,
    var followers:Int//"followers": 5,
    var following:Int//"following": 8,
    var created_at:String//"created_at": "2010-04-18T07:09:34Z",
}
