//
//  MasterTableCell.swift
//  GithubScreening
//
//  Created by Wiz on 3/29/20.
//  Copyright © 2020 Owner. All rights reserved.
//

import Foundation
import UIKit
class MasterTableCell : UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var repocountLbl: UILabel!
    @IBOutlet weak var avatarImage: LazyImageView!
}
