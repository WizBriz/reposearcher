//
//  DetailTableCell.swift
//  GithubScreening
//
//  Created by Wiz on 3/29/20.
//  Copyright © 2020 Owner. All rights reserved.
//

import Foundation
import UIKit
class DetailTableCell : UITableViewCell {
    @IBOutlet weak var repoName: UILabel!
    @IBOutlet weak var forkCount: UILabel!
    @IBOutlet weak var starCount: UILabel!
}
