//
//  DetailViewController.swift
//  GithubScreening
//
//  Created by Wiz on 3/29/20.
//  Copyright © 2020 Owner. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var loginName: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var joiningDate: UILabel!
    @IBOutlet weak var followers: UILabel!
    @IBOutlet weak var following: UILabel!
    @IBOutlet weak var bio: UILabel!
    
    @IBOutlet weak var avatarImage: LazyImageView!
    @IBOutlet weak var tableView: UITableView!
    var user : User?
    var repodata : [Repo]?
    var profile : Profile?
    func configureView() {
        if let user = user {
            self.loginName.text = user.login
            if let url = URL(string: user.avatar_url) {
                self.avatarImage.downloadImage(url: url, imageId: user.id)
               getProfileData ()
            }
        }
    }
    func getRepositoryData () {
        if let repo_url = user?.repos_url {
            NetworekManager.shared.getData(url: repo_url) { (data, response, error) in
                if error == nil {
                    if let jsonData = data {
                        do {
                            let repoRecords = try JSONDecoder().decode([Repo].self, from: jsonData)
                            DispatchQueue.main.async {
                                self.repodata = repoRecords
                                self.tableView.reloadData()
                            }
                        } catch {
                            print(error.localizedDescription)
                            self.repodata = nil
                        }
                    }
                }
            }
        }
    }
    func getProfileData () {
        if let profile_url = user?.url {
            NetworekManager.shared.getData(url: profile_url) { (data, response, error) in
                if error == nil {
                    if let jsonData = data {
                        do {
                            let profileData = try JSONDecoder().decode(Profile.self, from: jsonData)
                            DispatchQueue.main.async {
                                self.profile = profileData
                                self.cponfigureProfileDetails()
                                self.getRepositoryData()
                            }
                        } catch {
                            print(error.localizedDescription)
                            self.repodata = nil
                        }
                    }
                }
            }
        }
    }
    func cponfigureProfileDetails() {
        if self.profile != nil {
            setLabelText(string: profile?.email, label: self.email)
            setLabelText(string: profile?.location  , label: self.location)
            setLabelText(string: profile?.created_at  , label: self.joiningDate)
            if let followers = profile?.followers {
                setLabelText(string: "\(followers) Followers"  , label: self.followers)
            }
            if let following = profile?.following {
                setLabelText(string: "Following \(following)"  , label: self.following)
            }
            setLabelText(string: profile?.bio  , label: self.bio)
        }
    }
    func setLabelText(string : String?, label : UILabel) {
        if let _ = string{
            label.isHidden = false
            label.text = string!
        } else {
            label.isHidden = true
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
    }
    func prepareCell(cell: DetailTableCell, repo : Repo) {
        cell.repoName.text = repo.name
        cell.forkCount.text = "\(repo.forks_count)"
        cell.starCount.text = "\(repo.stargazers_count)"
    }
}
extension DetailViewController : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.repodata?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DetailTableCell
        let object = self.repodata?[indexPath.row]
        self.prepareCell(cell: cell, repo: object!)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let url = self.repodata?[indexPath.row].html_url {
            UIApplication.shared.openURL(NSURL(string: url) as! URL)
        }
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Public Repository"
    }
}
