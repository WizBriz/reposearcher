//
//  lazyImage.swift
//  GithubScreening
//
//  Created by Wiz on 3/29/20.
//  Copyright © 2020 Owner. All rights reserved.
//

import Foundation
import UIKit
class LazyImageView : UIImageView {
    static let imageCache = NSCache<NSString, UIImage>()
    var imageDownloadTask : URLSessionDataTask?
    
    func downloadImage(url : URL, imageId : Int) {
        self.image = nil
        if imageDownloadTask != nil {
            imageDownloadTask?.cancel()
        }
        if let image = LazyImageView.imageCache.object(forKey: "\(imageId)" as NSString) {
            self.image = image
        } else {
            self.imageDownloadTask = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                if error == nil {
                    DispatchQueue.main.async {
                        if let imageData = data {
                            if let image = UIImage(data: imageData) {
                                self.image = image
                                LazyImageView.imageCache.setObject(image, forKey: "\(imageId)" as NSString)
                            }
                        }
                    }
                }
            })
            self.imageDownloadTask?.resume()
        }
    }
}
