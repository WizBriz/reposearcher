//
//  MasterViewController.swift
//  GithubScreening
//
//  Created by Wiz on 3/29/20.
//  Copyright © 2020 Owner. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController,UISearchControllerDelegate {
    var users : Users?
    var detailViewController: DetailViewController? = nil
    var searchController = UISearchController(searchResultsController: nil)
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        self.searchController.searchBar.delegate = self
        self.searchController.obscuresBackgroundDuringPresentation = false
        self.searchController.searchBar.placeholder = "Search github users"
        self.searchController.searchBar.delegate = self
        self.navigationItem.searchController = searchController
        self.title = "Git User Search"
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    // MARK: - Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let object = users?.items[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.user = object
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
                detailViewController = controller
            }
        }
    }
    // MARK: - Table View
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = self.users?.items.count {
            return count
        }
        return  0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MasterTableCell
        let object = self.users?.items[indexPath.row]
        self.prepareCell(cell: cell, user: object!)
        return cell
    }
    
    func prepareCell(cell: MasterTableCell, user : User) {
        cell.titleLbl.text = user.login
        cell.repocountLbl.text = "\(0)"
        if let url = URL(string: user.avatar_url) {
            cell.avatarImage.downloadImage(url: url, imageId: user.id)
        }
    }
}

extension MasterViewController : UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = searchBar.text ?? ""

        // attempt to read the range they are trying to change, or exit if we can't
        guard let stringRange = Range(range, in: currentText) else { return false }

        // add their new text to the existing text
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        if(updatedText.count > 2) {
            UserDataSource.shared.searchUser(keyword: updatedText) { (users) in
                DispatchQueue.main.async {
                    self.users = users
                    self.tableView.reloadData()
                }
            }
        }
        return true
    }
}

extension MasterViewController {
    override func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        print("end accelrating")
    }
}
