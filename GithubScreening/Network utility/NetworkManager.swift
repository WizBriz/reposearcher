//
//  NetworkManager.swift
//  GithubScreening
//
//  Created by Wiz on 3/29/20.
//  Copyright © 2020 Owner. All rights reserved.
//

import Foundation


class NetworekManager {
    
    var dataTask : URLSessionDataTask?
    private init() {
        
    }
    public static var shared : NetworekManager {
        let networkManager = NetworekManager()
        return networkManager
    }
    func getData(url :String, completionHandler : @escaping (Data?, URLResponse?, Error?) -> Void) {
        if dataTask != nil {
            dataTask?.cancel()
        }
        if let url = URL(string: url){
            dataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
               completionHandler(data,response,error)
            }
        }
        dataTask?.resume()
    }
    
}
